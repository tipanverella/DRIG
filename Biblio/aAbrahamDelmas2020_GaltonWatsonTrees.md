# [An Introduction to Galton-Watson Trees and their local limits](https://arxiv.org/abs/1506.05571)

A lecture to give an overview of results on Galton-Watson trees.

## The Galton-Watson Process
Let $`\zeta`$ be a random variable taking values in $`\mathbb{N}`$ with distribution $`p = (p(n), n\in \mathbb{N})\colon p(n) = \mathbb{P}(\zeta = n)`$.  
The Galton-Watson process $`Z = (Z_n, n \in \mathbb{N})`$ with offspring distribution $`p`$ can be defined formally as follows.
Let $`(\zeta_{i,n}\colon i \in \mathbb{N}, n \in \mathbb{N})`$ be independent random variables distributed as $`\zeta`$.  
We set $`Z_0=1`$ and, for $`n \in \mathbb{N}^*`$:
```math
    Z_n = \sum_{i=1}^{Z_{n-1}} \zeta_{i,n}.
```
- The extinction event $`\mathcal{E}`$ corresponds to:
```math
    \mathcal{E} = \left\{\exists n \in \mathbb{N} \textrm{ s.t. } Z_n = 0\right\} = \lim_{n\to \infty} \left\{Z_n = 0\right\}.
```

### The set of discrete trees
